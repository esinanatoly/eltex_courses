#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/sem.h>
#include <sys/shm.h>

int sem_create();
void *shm_init(size_t size,int *shmid);
void deinit(int shmid,int semid);
int bee(int semid,int *honey,int bee_impact);
int bear(int semid,int *honey,int food);

int main(int argc,char *argv[])
 {
  int N=atoi(argv[1]);
  pid_t childpid[N];
  int shmid;
  int semid = sem_create();
  int *honey = shm_init(sizeof(int),&shmid);
  *honey = 0;

for(int i=0;i<=N;i++)
 {
    childpid[i] = fork();
    if(childpid[i] == 0)
     {
        if(i == 0)
          {
       	   bear(semid,honey,atoi(argv[3]));
       	   return 0;
          } 
          else
         {   
          bee(semid,honey,atoi(argv[2]));
         }              
     } 
 }
  waitpid(childpid[0],NULL,0);
  for(int i=1; i<=N; i++)
      {
       kill(childpid[i],SIGTERM);
       printf("Пчела убита PID=%d\n",childpid[i]);
      }
  deinit(shmid,semid);
  exit(0);
 }

int bee(int semid,int *honey,int bee_impact)
 { 
  struct sembuf lock_res = {0, -1, 0};
  struct sembuf ulock_res = {0, 1, 0};
  while(1)
   {
   	sleep(rand()%3+1);
    semop(semid,&lock_res,1);
    *honey +=bee_impact;
    printf("Пчела %d принесла %d. Всего меда %d \n",getpid(),bee_impact,*honey);    
   	semop(semid,&ulock_res,1);
   }
 }

int bear(int semid,int *honey,int food)
 { 
  int bearstatus=0;	
  struct sembuf lock_res = {0, -1, 0};
  struct sembuf ulock_res = {0, 1, 0};
  while(1)
   {
   	sleep(5);
    semop(semid,&lock_res,1);
    if(*honey>=food)
       {
        *honey-=food;
         printf("Медведь поел.Осталось меда = %d\n",*honey);
        bearstatus=0;
        semop(semid,&ulock_res,1);
       } 
    else
       {
         bearstatus++;
         switch(bearstatus)
          {
           case 1:
             printf("Медведь голодный пошел спать\n");
             semop(semid,&ulock_res,1);
           break;
           case 2:
             printf("Недостаточно меда,Медведь умер\n");
             semop(semid,&ulock_res,1);   
             return 0;
           break;  
           default:
           break; 
          }
       }
   }
}

int sem_create()
  {
    union semun 
    {
     int val;
     struct semid_ds *buf;
     unsigned short *array;
     struct seminfo *__buf; 
    };

    int semid;
    semid = semget(IPC_PRIVATE, 1, 0666 | IPC_CREAT);
    printf("Создан семофор : %d\n",semid);
    union semun arg;
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);
    return semid;
   }

void *shm_init(size_t size,int *shmid)
 {
   *shmid = shmget(IPC_PRIVATE,size,0666 | IPC_CREAT);
   printf("Общая память выделена : %d\n",*shmid);
   void *ptr = shmat(*shmid,NULL, 0);
   return ptr;
 }

void deinit(int shmid,int semid)
 {
    semctl(semid, 0, IPC_RMID);
    shmctl(shmid, IPC_RMID, NULL);
 }