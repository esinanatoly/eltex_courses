#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h> //для flock
#include <string.h>


int main (int argc,char *argv[])
 {
    //pid_t p[BEE_NUMBER];
    pid_t p[atoi(argv[1])];
    FILE *ppf;
    if ((ppf=fopen("honey.txt","w+"))==NULL)
     {
        printf("Ошибка открытия файла");
        exit(1);
     }
    rewind(ppf);
    fprintf(ppf,"%d",0);
    rewind(ppf);
    fclose(ppf);
    for(int i=0; i<=(atoi(argv[1])); i++)
       {
         p[i]=fork();
         if(p[i] == 0)
          { // Ребенок в цикле
           if(i == 0)
           {
            execl("./bear","bear",argv[3], NULL);
           } 
            execl("./bee","bee",argv[2], NULL);
          }
       }
    // Родитель вне цикла
    waitpid(p[0],NULL,0);
    for(int i=1; i<=(atoi(argv[1])); i++)
      {
      kill(p[i],SIGTERM);
      printf("Bee killed PID=%d\n",p[i]);
      }
    return 0;
 }

