#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>

typedef struct data 
  {
   int *honey;
   int impact;
  } data;


static pthread_mutex_t shared_mutex  = PTHREAD_MUTEX_INITIALIZER;

void *bee(void *arg)
 { 
 	data *data_bee = (data *) arg;
    int *honey=data_bee->honey;
    int impact=data_bee->impact;
    while(1)
      {
    	sleep(3);
        pthread_mutex_lock(&shared_mutex);   
        *honey +=impact;
        printf ("Пчела %x принесла 10 меда.Всего меда %d\n",pthread_self(),*honey);
        pthread_mutex_unlock(&shared_mutex);
     }
 };

 void *bear(void *arg)
 { 
 	int bearstatus=0;
    data *data_bear = (data *)arg;
    int *honey=data_bear->honey;
    int food=data_bear->impact;
    while(1)
       {
   	    sleep(5);
        pthread_mutex_lock(&shared_mutex);
        if(*honey>=food)
         {
           *honey-=food;
            printf("Медведь поел.Осталось меда = %d\n",*honey);
            bearstatus=0;
            pthread_mutex_unlock(&shared_mutex);
          } 
        else
          {
           bearstatus++;
           switch(bearstatus)
             {
             case 1:
               printf("Медведь голодный пошел спать\n");
               pthread_mutex_unlock(&shared_mutex);
             break;
             case 2:
               printf("Недостаточно меда\n");
               pthread_mutex_unlock(&shared_mutex);   
             return 0;
             break;  
             default:
             break; 
            }
         }
       }	
 };


 void wait_before_bear_dies (pthread_t *tid,int n)
 {
    pthread_join(tid[0],NULL);
    printf("Медведь умер\n");
    for (int i=1;i<=n;i++)
      {
        pthread_cancel(tid[i]);
        printf("Пчела номер [%d] убита\n",i);
    }
};



int main(int argc,char *argv[])
{
 int N = atoi(argv[1]);	
 int honey=0;
 data data_bee = {&honey,atoi(argv[2])};
 data data_bear = {&honey,atoi(argv[3])};
 pthread_t tid[N+1];
 for(int i=0;i<=N;i++)
    {
      if(i==0)
       {
         printf("Появился медведь\n");
         pthread_create(&tid[i], NULL, bear, &data_bear);
       }
      else
       {
         printf("Пчела номер %d начала работу\n",i);
         pthread_create(&tid[i], NULL, bee, &data_bee);
       }
    }    
 wait_before_bear_dies(&tid,N);
 exit(0);
}