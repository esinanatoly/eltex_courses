#include <stdio.h> 
#include <stdlib.h>
#include <dlfcn.h>
int main(int argc,char *args[]) 
 {  
  void *handler_lib;
  float (*pcube)(float);
  float (*pfourth)(float);
  handler_lib = dlopen("/home/linuxoid/eltex_courses/cpp_folder/laba5/libs/libfsdin1.so",RTLD_LAZY);
  if (!handler_lib)
    { 
     fprintf(stderr,"dlopen() error: %s\n", dlerror());
     return 1; 
    };
  pcube = dlsym(handler_lib,"cube");
  pfourth = dlsym(handler_lib,"fourth");
  printf("cube = %f\n",(*pcube)(atof(args[1]))); 
  printf("fourth = %f\n",(*pfourth)(atof(args[1]))); 
  dlclose(handler_lib);
  return 0; 
 }