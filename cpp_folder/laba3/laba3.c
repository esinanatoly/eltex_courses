#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio_ext.h>

typedef struct date 
  {
  int day;
  int month;
  int year;
  } date; // объявление нового типа данных "дата

typedef struct client 
  {
   char *name;
   int numbers;
   float sum;
   date d;
  } client; // объявление нового типа данных "клиент"

int compare(const void * first, const void * second);
void memoryfree(client **pp,int *pnumber);
void output (client **sc, int num);
void input (client **sc, int num); 

int main(void)
 {
  int num;//число покупателей	
  printf("Введите кол-в покупателей\n");
  scanf("%d",&num);	
  getchar();
  client **sc=(client**)malloc(sizeof(client*)*num);
  input(sc,num);// функция информации в структуры
  printf("До группировки по месяцу покупки \n");
  output(sc,num);
  // Сортировка
  qsort(sc,num,sizeof(client *), compare);
  //вывод отсортированных строк
  printf("После группировки по месяцу покупки \n");
  output(sc,num);
  memoryfree(sc,&num); //очистка выделенной памяти
  return 0;
 }

void input (client **sc, int num)
{
for (int n=0;n<num;n++)
 {	
	char buff[50];
	printf("Введите название покупки№:%d\n",n+1);
	for(int i=0; i<50; i++)
	   {
	    scanf("%c", &buff[i]);
	    if( buff[i] == '\n' ) 
	      {
	        buff[i] = '\0';
		    break;
	      }
	   }
	int sizestr=strlen(buff);
	sc[n]=(client*)malloc(sizeof(client));
	sc[n]->name=(char*)malloc(sizeof(char)*sizestr);
	strncpy(sc[n]->name,buff,sizestr);
	__fpurge(stdin);
	printf("Введите кол-во товаров\n");
	scanf("%d",&sc[n]->numbers);
	printf("Введите cумму\n");
	scanf("%f",&sc[n]->sum);
	getchar();
	printf("Введите дату в формате дд.мм.гггг:\n");
	scanf("%d%*c%d%*c%d",&sc[n]->d.day,&sc[n]->d.month,&sc[n]->d.year);
	getchar();
 }
}

void output (client **sc, int num)
 {
  for(int i=0;i<num;i++)
   {
    printf("Наименование:%s  ""Кол-во:%d   ""Сумма:%f   " "Дата покупки:%d.%d.%d\n"
    ,sc[i]->name,sc[i]->numbers,sc[i]->sum,sc[i]->d.day,sc[i]->d.month,sc[i]->d.year);
   }
 }

int compare(const void * first, const void * second)
 {
    client*fir=*(client **)first;
    client *sec=*(client **)second;
    return (fir->d.month-sec->d.month);
 }

void memoryfree(client **pp,int *pnumber)
 {
   for (int i=0;i<*pnumber;i++)
      {
       free(pp[i]);
      }
   free(pp);
 }