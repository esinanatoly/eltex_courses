#define UDP_PORT1 4900
#include "client.h"


int main (void)
{
    srand(time(NULL));
	int udp_sock, recvStringLen;
	char recvString[MAXRECVSTRING];
	struct sockaddr_storage their_addr;
    socklen_t addr_len;
    addr_len=sizeof(their_addr);
  	while(1)
    {
        //создаем UDP-сокет, привязываемся к порту
        udp_sock=UDP_Init(UDP_PORT1);
       	printf("Client %d: waiting UDP notification...\n",getpid());
        // ожидаем UDP-cooбщение
        if ((recvStringLen = recvfrom(udp_sock, recvString, MAXRECVSTRING,0,
			(struct sockaddr *)&their_addr, &addr_len)) < 0)
        {	perror("Client: error in recvfrom()");	}
        else //если приняли
        {
            short unsigned ServTCPport=atoi(recvString); // номер порта, принятый от сервера через сообщение
            char ipstr[INET_ADDRSTRLEN]; // IP-адрес сервера
            inet_ntop(their_addr.ss_family,get_in_addr((struct sockaddr *)&their_addr), ipstr, sizeof(ipstr));
            printf("Client %d received TCP port: %d, %d bytes, IP %s \n",getpid(), ServTCPport, recvStringLen, ipstr);
            close(udp_sock); //закрываем UDP-соединение

            //подключаемся к TCP
            int tcp_sock = TCP_Init(ServTCPport, ipstr);

            //формируем заявку серверу
            char sendString[MAXSENDSTRING];
            sprintf(sendString, "%d", getpid());
            int sendStringLen=sizeof(sendString);
            if ((send(tcp_sock, &sendString, sendStringLen,0))!=sendStringLen)
            {	perror("Client: send() failed");	}
            printf("Client %d send TCP msg (own PID):  %s\n",getpid(), sendString);
            close(tcp_sock);
        }
        printf("Client %d sleep\n\n", getpid());
        sleep(rand()%SLEEPTIME);
    }
	return EXIT_SUCCESS;
}