#include "server.h"

int get_sem_val(int semid)
{
    return(semctl(semid,0,GETVAL,0));
}

int sem_create() // инициализирцем семафор
  {
    union semun 
     {
      int val;
      struct semid_ds *buf;
      unsigned short *array;
      struct seminfo *__buf; 
     };

     int semid;
     semid = semget(IPC_PRIVATE, 1, 0666 | IPC_CREAT);
     printf("Создан семофор : %d\n",semid);
     union semun arg;
     arg.val = QUEUE;
     semctl(semid, 0, SETVAL, arg);
     return semid;
  }
  
int msg_create() // инициализация очереди сообщений
  {
   int mqid;
   int msgflg = 0600 | IPC_CREAT;
   if( (mqid = msgget(IPC_PRIVATE,msgflg)) >= 0 )
      printf("Создана очередь сообщений id = %d\n",mqid);
   else
      return -1;
  return mqid;
  } 



int CreateTCPServerSocket(unsigned short port)
  {
    int sock; 
    struct sockaddr_in ServAddr; 
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
      {	
    	perror("Сервер: ошибка при создании TCP socket()");
    	exit(1);	
      }
    memset(&ServAddr, 0, sizeof(ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    ServAddr.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
      {	
    	perror("Сервер: ошибка при связывании порта и адреса bind()");
    	exit(1);
      }
    if (listen(sock, QUEUE) < 0)
      {	
    	perror("Сервер: ошибка при listen()");
    	exit(1);	
      }
    return sock;
  }  

int AcceptTCP(int servSock)
  {
    int clntSock; 
    struct sockaddr_in ClntAddr; 
    unsigned int clntLen; 
    clntLen = sizeof(ClntAddr);
    if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr, &clntLen)) < 0)
      {	
    	perror("Сервер: ошика при accept()");
    	exit(1);	
      }
    printf("Сервер: создан сокет для клиента  %s\n", inet_ntoa(ClntAddr.sin_addr));
    return clntSock;
  }

 void * ClientThread(void * arg)
  {
	pthread_detach(pthread_self());
	int connSock=((client_str *)arg)->connSock;
	int msqid=((client_str *)arg)->msqid;
	int semid=((client_str *)arg)->semid;
	int type=((client_str *)arg)->type;
	free(arg);
	Serv_msg message;
	switch(type)
        {
         case 1:
             printf("Сервер: Client1 thread %lu\n", pthread_self());
             char recvString[MAXSTRING];
	         if ((recv(connSock, recvString, MAXSTRING,0))==-1)
	             { 
	               perror("Сервер: recv() failed");
		           exit (1);
		         }
	         printf("Сервер принял TCP сообщение:  %s\n",recvString);
	         close(connSock);
             //запись в очередь
             struct sembuf lock={0,-1,0}; //для блокировки ресурса
             if (semop(semid,&lock,1)==-1) //уменьшаем семафор
                { 
                  perror("Сервер: semlock failed"); 
                }
             message.mtype=MSGTYPE;
             strcpy(message.mdata,recvString);
             if (msgsnd(msqid,&message,MAXSTRING,0)<0)
                { 
                 perror("Сервер: msgsnd() faled");
                 exit(1);
                }
         break;
         case 2:
             printf("Сервер: Client2 thread %lu\n", pthread_self());
             if (msgrcv(msqid,&message,MAXSTRING,100,0)<0)
               {   
               	 perror("msgrcv");
                 exit(1); 
               }
	         char sendString[MAXSTRING];
	         strcpy(sendString,message.mdata);
	         int sendStringLen=sizeof(sendString);
	         if ((send(connSock, &sendString, sendStringLen,0))!=sendStringLen)
               {
             	 perror("Сервер: send() failed");
		         exit (1);
		       }
	         printf("Сервер отправил TCP сообщение:  %s\n",sendString);
	         struct sembuf unlock={0,1,0}; //для разблокировки ресурса
	         semop(semid,&unlock,1); //увеличиваем семафор
	         close(connSock);
         break;  
         default:
         break; 
        } 
    pthread_exit(NULL);
  }

 
void * ThreadTCP(void * arg)
  {
    printf("Сервер: TCP поток %lu\n", pthread_self());
    int type=((TCP_struct *)arg)->type;
    int msqid=((TCP_struct *)arg)->msqid;
    int semid=((TCP_struct *)arg)->semid;
    free(arg);
    unsigned short tcp_port;
    switch(type)
          {
             case 1:
                tcp_port=atoi(TCP1_PORT);
             break;
             case 2:
                tcp_port=atoi(TCP2_PORT);  
             break;  
             default:
             break; 
          } 
    int listenSock,connSock;
	listenSock=CreateTCPServerSocket(tcp_port);
	while(1)
	 {	
	 	connSock=AcceptTCP(listenSock);
        pthread_t client_tid;
        client_str *client_arg;
        client_arg=(client_str *)malloc(sizeof(client_str));
        if (client_arg == NULL)
           {	
        	perror("Сервер: ошибка выделения памяти  malloc");
			exit(1);	
		   }
		client_arg->connSock=connSock;
		client_arg->msqid=msqid;
		client_arg->semid=semid;
		client_arg->type=type;
        if (pthread_create(&client_tid, NULL, ClientThread, (void *)client_arg)!=0)
          {
           perror("Сервер: ошибка при создании потока pthread_create");
           exit(1);	
          }  
	 }
   pthread_exit(NULL);
  }
 
void * ThreadUDP(void * arg)
  {
    int UDP_PORT=((UDP_struct *)arg)->udp_p;
	char *broadcast_msg=((UDP_struct *)arg)->tcp_p;
	int semid=((UDP_struct *)arg)->semid;
	int type=((UDP_struct *)arg)->type;
	free(arg);
	printf("Сервер: UDP%d port %d\n", type, UDP_PORT);
	int udp_sock;
	int broadcastPermission;
	struct sockaddr_in broadcastaddr;
	memset(&broadcastaddr, 0, sizeof(broadcastaddr));
	broadcastaddr.sin_family = AF_INET;
	broadcastaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	broadcastaddr.sin_port = htons(UDP_PORT);
	if ((udp_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0)
	   {	
		perror("Сервер: ошибка при создании UDP сокета socket()");
		exit(1);
	   }
	broadcastPermission=1;
	if (setsockopt(udp_sock, SOL_SOCKET, SO_BROADCAST,(void *)&broadcastPermission, sizeof(broadcastPermission)) == -1)
       { 
	    perror("Сервер: ошибка в setsockopt()");
		exit(1);	
	   }
	while(1)
	{
     switch(type)
          {
           case 1:
                if(get_sem_val(semid)) //в очереди есть места
                  {
                   if((sendto(udp_sock, broadcast_msg, strlen(broadcast_msg), 0,
				   (struct sockaddr *)&broadcastaddr, sizeof(broadcastaddr)))!=strlen(broadcast_msg))
                    {	
                     perror("Сервер: ошибка  sendto()");
                     exit(1);	
                    }
                   printf("Сервер: UDP%d послал широковещательное сообщение\n", type);
                   sleep(UDP_SLEEP); 
                  }
           break;
           case 2:
                 if(get_sem_val(semid)<QUEUE) //в очереди есть сообщения
                  {
                   if((sendto(udp_sock, broadcast_msg, strlen(broadcast_msg), 0,
				   (struct sockaddr *)&broadcastaddr, sizeof(broadcastaddr)))!=strlen(broadcast_msg))
					{	
					  perror("Сервер: ошибка  sendto()");
                      exit(1);	
                     }
                   printf("Сервер: UDP%d послал широковещательное сообщение\n", type);
                   sleep(UDP_SLEEP);
                  } 
           break;  
           default:
           break; 
          } 
	}
	//недостижимый участок кода
	close(udp_sock);
	pthread_exit(NULL);

  }

void deinit(int mqid,int semid) // удаляем очередь и семафор
 {
 	int c;
 	while(1)
    {   c = getc(stdin);
		switch(c)
		{   case 'q':
           if(semctl(semid, 0, IPC_RMID) >= 0)
              printf("Удален семафор %d\n",semid);
           if(msgctl(mqid, IPC_RMID, NULL)>=0)
              printf("Удалена очередь %d\n",mqid);
           raise(SIGTERM);
          break;
          default:
          break; 
        }
    }    
 }