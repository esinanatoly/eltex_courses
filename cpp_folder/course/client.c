#include "client.h"

void *get_in_addr(struct sockaddr *sa)
{	return &(((struct sockaddr_in*)sa)->sin_addr);	}

int UDP_Init(int UDP_PORT)
{
    int udp_sock;
    struct sockaddr_in broadcast_addr;
	int ServUDPport=UDP_PORT;
	memset(&broadcast_addr, '0', sizeof(broadcast_addr));
	broadcast_addr.sin_family = AF_INET;
	broadcast_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	broadcast_addr.sin_port = htons(ServUDPport);
	int mark;
	do
	{	//создание UDP сокета
		if ((udp_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0)
		{	perror("Client: error in socket()");	}
		//привязка сокета к адресу
		mark=bind(udp_sock, (struct sockaddr *)&broadcast_addr, sizeof(broadcast_addr));
		if(mark<0) //если привязать сокет к адресу не удалось
		{	close(udp_sock);
			if (errno==EADDRINUSE)
			{	ServUDPport++;
				broadcast_addr.sin_port = htons(ServUDPport);	}
			else
			{	perror("Client: error in bind()");	}	}
		else //если удалось привязать сокет к адресу
		{	printf("Client %d: bind to UDP port %d\n",getpid(),ServUDPport);	}
	}	while(mark<0);
	return udp_sock;
}

int TCP_Init(int ServTCPport, char *ipstr)
{
    int tcp_sock;
    struct sockaddr_in ServAddr;
    memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_addr.s_addr = inet_addr(ipstr);
	ServAddr.sin_port = htons(ServTCPport);
	if ((tcp_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{	perror("Client: error in socket()");
		return EXIT_FAILURE;	}
	if (connect(tcp_sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
	{	perror("Client: error in connect");
		return EXIT_FAILURE;	}
    return tcp_sock;
}