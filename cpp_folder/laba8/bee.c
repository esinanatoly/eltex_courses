#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

struct mymesg
 {
  long mesg_type;
  int mesg_data;
  pid_t pid;
 };
 
void main(int argc,char *argv[])
{
 printf("Пчела %d начала работу\n",getpid());
 struct mymesg bufbee;
 bufbee.mesg_data=atoi(argv[2]);;
 bufbee.mesg_type = atol(argv[3]);
 bufbee.pid = getpid();
 while(1)
   {
     srand(clock());
     sleep(rand()%5+1);
     msgsnd(atoi(argv[1]), &bufbee,sizeof(bufbee),0);
   }
}