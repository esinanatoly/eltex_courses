#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

struct mymesg
 {
  long mesg_type;
  int mesg_data;
  pid_t pid;
 };

void main(int argc,char *argv[])
{
   printf("Появился медведь\n");
   struct mymesg bufbear;
   bufbear.mesg_data=atoi(argv[2]);
   bufbear.mesg_type = atol(argv[3]);
   bufbear.pid = getpid();
   while(1)
      {
        sleep(5);
        msgsnd(atoi(argv[1]), &bufbear,sizeof(bufbear),0);
      }
}