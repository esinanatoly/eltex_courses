#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

struct mymesg
 {
  long mesg_type;
  int mesg_data;
  pid_t pid;
 };
int main(int argc,char *argv[])
{
 int honey = 0;
 int N = atoi(argv[1]);
 int food =atoi(argv[3]);
 int mqid;
 int msgflg = 0600 | IPC_CREAT;
 long type = 10;
 int bearstatus=0;
 int status;
 pid_t childpid[N];
 char args1[10];
 char args2[10];

 if( (mqid = msgget(IPC_PRIVATE,msgflg)) >= 0 )
    printf("mqid = %d\n",mqid);
 else
    return -1;

 sprintf(args1,"%d",mqid);
 sprintf(args2,"%ld",type);

 for(int i=0;i<=N;i++)
 {
    childpid[i] = fork();
    if(childpid[i] == 0)
    {
       if(i==0)
        {
       	 execl("./bear","bear",args1,argv[3],args2,NULL);
        } 
        else
         {   
          execl("./bee","bee",args1,argv[2],args2,NULL);
         }              
    } 
  }

 struct mymesg buf_rcv;
 while(1)
 {
    msgrcv(mqid,&buf_rcv,sizeof(buf_rcv),type,0);
   if(buf_rcv.pid==childpid[0])
    {
      if(honey>=food)
       {
        honey-=food;
        printf("Медведь поел.Осталось меда = %d\n",honey);
        bearstatus=0;
       } 
       else
       {
         bearstatus++;
         switch(bearstatus)
          {
           case 1:
           printf("Медведь голодный пошел спать\n");
           break;
           case 2:
           printf("Недостаточно меда\n");
           for(int i=0;i<=N;i++)
            {
             kill(childpid[i],SIGTERM);
             if(i==0)
              printf("Медведь %d убит\n",childpid[i]);
             else
              printf("Пчела %d убита\n",childpid[i]);
            }
            if( (status = msgctl(mqid, IPC_RMID, NULL)) >= 0 )
               printf("status = %d\n",status);
            else
             return -1;
            return 0;
           default:
           break; 
          }

       }	
    }
    else
    {	
     honey+= buf_rcv.mesg_data;
     printf("Пчела %d принела меда %d. Всего меда =%d\n",buf_rcv.pid,buf_rcv.mesg_data,honey);
    }
 }
}
