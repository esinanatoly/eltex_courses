#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

int main(int argc,char *argv[])
	{
	 //printf("check point 1\n");
	 int pipe_bee[atoi(argv[1])+1][2];
	 int pipe_bear[3][2];
	 int honey =0 ;
	 int buf;
	 int bear_status;
	 int child[atoi(argv[1])+1];
	 int flags;
	 int access;
	 int back_honey;
	 int bee_impact=5;
   char bear_args1[10];
   char bear_args2[10];
   char bear_args3[10];
   char bear_args4[10];
   char bee_args1[10];
   char bee_args2[10];
   char bee_args3[10];
	// printf("check point 1\n");
	 for(int i=0;i<3;i++)
	    {	
	      pipe(pipe_bear[i]);	
	    }   
   sprintf(bear_args1,"%d",pipe_bear[0][0]);
   sprintf(bear_args2,"%d",pipe_bear[1][1]);
   sprintf(bear_args3,"%d",pipe_bear[2][1]); 
     for(int i=0;i<atoi(argv[1])+1;i++)
        {
         pipe(pipe_bee[i]);
         sprintf(bee_args1,"%d",i);
         sprintf(bee_args2,"%d",pipe_bee[i][1]); 
         child[i]=fork();
         if (child[i]==0)
     	   { 
     	    if(i!=0)
     	   	 {
            close(pipe_bee[i][0]);
            execl("./bee","bee",bee_args1,bee_args2,argv[2],NULL);
             }
              close(pipe_bear[0][1]);
              close(pipe_bear[1][0]);
              close(pipe_bear[2][0]);
              execl("./bear","bear",bear_args1,bear_args2,bear_args3,argv[3],NULL);
          }
        }   
    close(pipe_bear[0][0]);
    close(pipe_bear[1][1]); 
    close(pipe_bear[2][1]);
    if((flags=fcntl(pipe_bear[2][0],F_GETFL,0))<0)
        			perror("F_GETFL error");
        	flags |= O_NONBLOCK;
    if(fcntl(pipe_bear[2][0],F_SETFL,flags)<0)
        			perror("F_SETFL error");  
    for (int i=1;i<atoi(argv[1])+1;i++)
        {    
         	close(pipe_bee[i][1]);
        	if((flags=fcntl(pipe_bee[i][0],F_GETFL,0))<0)
        			perror("F_GETFL error");
        	flags |= O_NONBLOCK;
        	if(fcntl(pipe_bee[i][0],F_SETFL,flags)<0)
        			perror("F_SETFL error");
        }
    while(1)
    	{
    if(read(pipe_bear[2][0],&bear_status,sizeof(bear_status))>0)
       {		
       access=bear_status;
       }
    else
       {      
       access=1;
       }
    	 switch(access)
    	  {
    	    case 1:	
      	  		for(int i=1;i<atoi(argv[1])+1;i++)
      	    	{	
             		if(read(pipe_bee[i][0],&buf,sizeof(buf))>0)
              		{
                 		honey+=buf;
                 		printf("Пчела №%d  принесла меда %d\n",i,buf);
                 		printf("Всего меда  %d\n",honey); 
                	}
            	}
          		break;
          	case 0:
             	write(pipe_bear[0][1],&honey,sizeof(honey));
             	read(pipe_bear[1][0],&back_honey,sizeof(back_honey));
             	if(back_honey>0)
              	{	
               		honey=back_honey;
               		printf("Медведь поел.Осталось меда %d\n",honey);
              	} 
             	break;
          	case 2:
              	for(int i=1; i<atoi(argv[1])+1; i++)
               	{
                	kill(child[i],SIGTERM);
                	printf("Bee killed PID=%d\n",child[i]);
               	}
              return 0;	
          	default:
           		break;            
    	    }
        }
	
return 0;	
}	