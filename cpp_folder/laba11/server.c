#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <string.h> 
#include <pthread.h>
#include <errno.h> 
#include <sys/types.h> 

typedef struct data 
  {
   int listendes;
   char buff[80];
  } data;


#define NUM 5 //число клиентов
#define PORT 5000

void * thread(void *arg) 
 {
   int sum=0;
   int connfd=0;
   char buffer[80];
   data *data_thread = (data *) arg;
   int listendes=data_thread->listendes;
   sprintf(buffer,"%s",data_thread->buff);
   connfd = accept(listendes, (struct sockaddr*)NULL ,NULL);
   write(connfd, buffer, sizeof(buffer));
   read(connfd,&sum,sizeof(int));
   printf("Контрольная сумма файла %s = %d\n",buffer,sum);
   close(connfd);
   return 0;
 }

 void wait_for_threads (pthread_t *tid)
 {
 	for(int i=0;i<NUM;i++)
    pthread_join(tid[i],NULL);
 };

void memoryfree(data **pp,int pnumber)
 {
   for (int i=0;i<pnumber;i++)
      {
       free(pp[i]);
      }
   free(pp);
 }

int main(int argc,char *argv[])
{
  int listenfd = 0,connfd = 0; // дескрипторы слушающего сокета и сокета для обмена с клиентом
  struct sockaddr_in serv_addr;
  //создание слушающего сокета
  if ((listenfd = socket(AF_INET, SOCK_STREAM, 0))<0) //TCP (потоковые сокеты)
   {
      printf("\n Error: Socket creation failed \n");
      char *p = strerror(errno);
      fprintf(stderr," Error: %s\n", p);
      return 1;
   }
  memset(&serv_addr,'0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(PORT);
  int yes=1;

  if (setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
    {
      perror("setsockopt");
      exit(1);
    }
   //привязка сокета к адресу
  if ((bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr)))<0)
  {
      printf("\n Error: Socket bind failed \n");
      char *p = strerror(errno);
      fprintf(stderr," Error: %s\n", p);
      return 1;
  }
  printf("Server start working\n");
  //ожидание подключения, размер очереди равен числу клиентов
  if(listen(listenfd, NUM) == -1){
      printf("Failed to listen\n");
      return -1;
  }
    pthread_t tid[NUM];
    data **sc=(data**)malloc(sizeof(data*)*NUM);
    for(int i=0;i<NUM;i++)
    {
    sc[i]=(data*)malloc(sizeof(data));
   	sc[i]->listendes=listenfd;
   	sprintf(sc[i]->buff,"%s",argv[i+1]);
    pthread_create(&tid[i], NULL, thread, sc[i]);
    }
    wait_for_threads(&tid);
    memoryfree(sc,NUM); 
    sleep(3);
    exit(0);
}