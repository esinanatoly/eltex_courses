#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define NUM 5
#define PORT 5000

void * client (void *num) 
{
  sleep(1);
  int n=*(int *)num; //номер клиента
  int sockfd = 0; //дескриптор сокета
  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  serv_addr.sin_port = htons(PORT);
  while(1)
     {
        if((sockfd = socket(AF_INET, SOCK_STREAM, 0))< 0)
          {
            printf("\n Error : Could not create socket \n");
            return -1;
          }
        if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
          {   //если соединение не установлено
            printf(" Error : Connect Failed \n");
            char *p = strerror(errno);
            fprintf(stderr," Error %s occured\n", p);
            sleep(3);
            continue;
          }
        else //если соединение установлено
          {
            
            int size;// размер для чтения имени файла
            char buff[80];
            read(sockfd,buff,80*sizeof(char));
            printf("Клиент получил файл %s\n",buff);
            FILE *fp1;
            int sum=0;
            int code=0;
            fp1=fopen(buff,"r");
            while((code=fgetc(fp1))!=EOF) 
             {
               sum+=code;  
             }
              fclose(fp1);
              write(sockfd,&sum,sizeof(int));
              close(sockfd);
              sleep(3);
              return 0;
            }
         }
}

int main(void)
{
    pthread_t pthid[NUM]; //массив идентификаторов потоков
    int n[NUM];
    for (int i=0; i<NUM; i++)
    {
      n[i]=i;
      pthread_create(&pthid[i],NULL,client,&n[i]);
    }
   for (int i=0; i<NUM; i++)
   {
       pthread_join(pthid[i],NULL);
   }
    return 0;
}

