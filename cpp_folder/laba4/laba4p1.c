#include <stdio.h>
#include <stdlib.h>
#include <string.h>

 int numspace(char *str);
 void main(int argc,char *args[])
  {
  FILE *fp1,*fp2;
  char str[256];
  char filename[64];
  memset(filename,0,sizeof(filename));
  sprintf(filename,"%s.txt",args[1]);
  // открываем файл на чтение
  fp1=fopen(args[1],"r");
  // открываем файл на запись
  fp2=fopen(filename,"w");
  
  while(!feof(fp1))
    {
     if(fgets(str,254,fp1))
     if(numspace(str)<atoi(args[2]))
       {
       	fputs(str,fp2);
       }
    }
   fclose(fp1);
   fclose(fp2); 
 }
int numspace(char *str) // функция определяет кол-во пробелов в строке
 {
 int numspc=0;
 int len=strlen(str);
 for(int i=0;i<len;i++)
    {
     if(str[i]==' ')
     numspc++;	
    }
 return numspc; 
 } 