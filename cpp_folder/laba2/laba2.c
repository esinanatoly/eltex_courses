#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include  <ctype.h>

int compare(const void * first, const void * second);
void inp_str(char **pp,int maxlen,int num);
void out_str(char **pp,int num);
void memoryfree(char **pp,int *pnumber);
int num_space(int sizestr,char *str);
unsigned N_change;//число перестановок

int main(void)
 {
   int maxlen;// максимальная длина строки
   int num;// кол-во строк
   printf("\nЛаба№2:Строки - Вариант№5 :\n\n");
   printf("Введите макс.длину строки:\n");
   scanf("%d", &maxlen);
   printf("Введите кол-во строк:\n");
   scanf("%d", &num);
   getchar();
   char **pp=(char **)malloc(sizeof(char *)*num);
    // ввод строк
	 inp_str(pp,maxlen,num);// функция ввода строк
   printf("Исходные строки\n");
   out_str(pp,num);// функция вывода строк
    // Сортировка
   qsort(pp,num,sizeof(char *), compare);
    //вывод отсортированных строк
   printf("Отсортированные строки\n");
   out_str(pp,num);// функция вывода строк
   printf("Кол-во перестановок:%d  Первый символ:%c\n",N_change,pp[num-1][0]); 
   memoryfree(pp,&num); //очистка выделенной памяти
   return 0;
 }


void inp_str(char **pp,int maxlen,int num)
 {
 for (int n=0;n<num;n++)
  {
   char *buff;
   buff=(char*)malloc(maxlen*sizeof(char));
   printf("Введите строку№:%d\n",num+1);
   for(int i=0; i<maxlen; i++)
     {
      scanf("%c", &buff[i]);
      if( buff[i] == '\n' ) 
        {
	      buff[i] = '\0';
	       break;
	      }
      }
   int sizestr=strlen(buff);
   pp[n]=(char*)malloc(sizeof(char)*sizestr);
   strncpy(pp[n],buff,sizestr);
   __fpurge(stdin);
   free(buff);  
  }
 }

void out_str(char **pp,int num)
 {
  for (int i=0;i<num;i++)
   {
    int sizestr=strlen(pp[i]);
    int num_spc=num_space(sizestr,pp[i]);
    printf("Строка№ %d:  Строка:%s Длина:%d  Кол-во слов:%d\n",i+1,pp[i],sizestr,num_spc);
   }
 }

void memoryfree(char **pp,int *pnumber)
  {
   for (int i=0;i<*pnumber;i++)
      {
       free(pp[i]);
      }
   free(pp);
  }

int compare(const void * first, const void * second)
 {
    N_change++;
    char *fir=*(char **)first;
    char *sec=*(char **)second;
    int fir_len=strlen(fir);
    int sec_len=strlen(sec);
    int fir_num=num_space(fir_len,fir);
    int sec_num=num_space(sec_len,sec);
    return (fir_num-sec_num);
 }

int num_space(int sizestr,char *str) // функция определяет количество слов в строке
{
  int num_spc=0;
  if(isalpha(str[0]))
    {
     num_spc++;
    }	
  for (int i=1;i<sizestr;i++)
     {
       if(!isalpha(str[i-1]) && isalpha(str[i]))
       num_spc++;
     }
  return num_spc; 
 }
