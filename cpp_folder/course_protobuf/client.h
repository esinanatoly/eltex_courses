#include <sys/types.h> //socket data types
#include <sys/socket.h> //socket(), connect(), send(), recv()
#include <netdb.h>
#include <stdio.h> //printf()
#include <stdlib.h>
#include <unistd.h> // close(), sleep()
#include <string.h> //memset()
#include <errno.h> //errno()
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h> // time()

//МАКРОСЫ
#define MAXRECVSTRING 255 //max число байт, принимаемое по UDP
#define MAXSENDSTRING 53 //max число байт, отправляемое по TCP (1 тип)
#define SLEEPTIME 9
#define MAXRECVREQUEST 255 //max число байт, принимаемое по TCP (2 тип)
#define MAX_UDPMSG_SIZE 127
#define MAX_TCPMSG_SIZE 127

//ПРОТОТИПЫ
void *get_in_addr(struct sockaddr *sa);
int UDP_Init(int UDP_PORT);
int TCP_Init(int ServTCPport, char *ipstr);
