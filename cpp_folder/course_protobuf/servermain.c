#include "server.h"

void main(int argc,char *argv[])
 {
  int semid = sem_create();// cоздаем семафор
  int mqid = msg_create();// создаем очередь
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
  pthread_t tcp1_tid;
     //TCP-поток для клиентов первого типа
    TCP_struct *tcp_arg1;
    tcp_arg1=(TCP_struct *)malloc(sizeof(TCP_struct));
    tcp_arg1->type=1;
    tcp_arg1->msqid=mqid;
    tcp_arg1->semid=semid;
    if(pthread_create(&tcp1_tid, &attr, ThreadTCP, (void *)tcp_arg1)!=0)
       {	
      	perror("Ошибка при создании потока TCPtype1");
        exit(1);
       }
    //TCP-поток для клиентов второго типа
    pthread_t tcp2_tid;
    TCP_struct *tcp_arg2;
    tcp_arg2=(TCP_struct *)malloc(sizeof(TCP_struct));
    tcp_arg2->type=2;
    tcp_arg2->msqid=mqid;
    tcp_arg2->semid=semid;
    if(pthread_create(&tcp2_tid, &attr, ThreadTCP, (void *)tcp_arg2)!=0)
      {	
      	perror("Ошибка при создании потока TCPtype2");
        exit(1);	
      }
    //UDP-потоки для клиентов первого типа
	pthread_t udp1_tid[CLIENT_TYPE1];
	int i=0;
	for (int u_port=UDP1_PORT; u_port<UDP1_PORT+CLIENT_TYPE1; u_port++)
	  {   
		UDP_struct *arg1=(UDP_struct *)malloc(sizeof(UDP_struct));
	    arg1->type=1;
	    arg1->udp_p=u_port;
	    arg1->tcp_p=TCP1_PORT;
	    arg1->semid=semid;
	    arg1->ipserv[16]=argv[1];
		if (pthread_create(&udp1_tid[i], &attr, ThreadUDP, (void *)arg1)!=0)
	   	   {	
			perror("Ошибка при создании потока UDPtype1");
			exit(1);
		   }
		++i;
	  }
    //UDP-потоки для клиентов второго типа
    pthread_t udp2_tid[CLIENT_TYPE2];
	i=0;
	for (int u_port=UDP2_PORT; u_port<UDP2_PORT+CLIENT_TYPE2; u_port++)
	  {  
	    UDP_struct *arg2=(UDP_struct *)malloc(sizeof(UDP_struct));
	    arg2->type=2;
	    arg2->udp_p=u_port;
	    arg2->tcp_p=TCP2_PORT;
	    arg2->semid=semid;
	    arg2->ipserv[16]=argv[1];
		if (pthread_create(&udp2_tid[i], &attr, ThreadUDP, (void *)arg2)!=0)
		   {	
			perror("Ошибка при создании потока UDPtype2");
			exit(1);	
		   }
		++i;
	  }
  deinit(mqid,semid);// удаляем очередь и семафор
  pthread_exit(NULL);
 }