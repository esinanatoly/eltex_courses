#! /bin/sh
add() # функция добавления будильника в расписание 
 {
   echo "Введите время "
   read hour min
   echo "Выберите мелодию"
   echo "Bring - 1
   Rebuild -2 "
   read num_sound      
   case $num_sound in
   1)
   sound=/home/linuxoid/scripts/laba2/bring.mp3
   ;;
   2)
   sound=/home/linuxoid/scripts/laba2/rebuild.mp3
   ;;
   esac      
   echo "$min $hour * * * mplayer $sound">>shedule.txt
   crontab /home/linuxoid/scripts/laba2/shedule.txt
 }

off() # функция отключения будильника
 { 
   pkill mplayer
 }

del()
 {
 echo "Введите время для удаления"
 read hour min
 crontab -l | grep -v "$min $hour" > shedule.txt
 crontab /home/linuxoid/scripts/laba2/shedule.txt
 }
  edit()
 {
  del
  add
  ./laba2.sh
 }
echo "Действующее расписание"
 crontab -l
echo "Выберете операцию для будильника:
      Добавить      - a
      Редактировать - e
      Удалить       - d
      Отключить     - k
      Очистить      - r 
      Выход         - q
"
read action
case $action in # взависимости от передаваемого параметра выбираем действие с будильником
d)
del
./laba2.sh
;;
a)
add
./laba2.sh
;;
e)
edit
./laba2.sh
;;
r)
crontab -r
./laba2.sh
;;
k)
off
./laba2.sh
;;
q)
cp /dev/null shedule.txt
exit
;;
*)
echo "Ошибка в названии операции"
./laba2.sh
;;
esac
